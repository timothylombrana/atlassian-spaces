# Atlassian - Spaces

## Installing

---

Run `npm i` from your terminal to install all dependencies.

## Development server

---

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Rest API mocking

---

Rest API mocking and intercepting is handled by [Beeceptor](https://beeceptor.com/) via a private paid account so NONE of the provided test data is open to the public. Replace the `BASE_URL` in the file `space.service.ts` and you are ready.

## How it works

---

-   **npm start**: After running `npm start` & navigating to `http://localhost:4200/`.
    The base of the app will redirect to all spaces which sets the default space to
    the first space from all returned spaces.
-   **loader**: I'm using the Atlassian logo as a custom loading overlay.
    On initial page load no opacity given and subsequent requests you'll
    notice a bit of opacity to give end user visual cue that current request is
    processing.
-   **spaces**: Each space has a title and a created by user name. Conditionally
    displays a description of the space if returned in request.
-   **tabs**: Tabs for assets & entries will conditionally be shown
    if respective data is in the request.
-   **table**: Table has client side sorting enabled for each column. With
    a default of 100 max table items to be shown.

## Running unit tests

---

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Design Guide

---

I tried matching the design and styles provided [Atlassian Design](https://atlassian.design/guidelines/marketing/overview).

## Author

---

[Timothy Lombraña](https://www.linkedin.com/in/tlombrana/)
