import * as _ from 'lodash';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { SpaceService } from './spaces/space.service';
import { MatSnackBarConfig, MatSnackBar } from '@angular/material';
import { NotificationService } from './notification.service';
import { SnackBarNotificationComponent } from './snack-bar-notification/snack-bar-notification.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    isLoading = true;
    spaces: Array<any> = [];

    constructor(
        private router: Router,
        private snackBar: MatSnackBar,
        private spaceService: SpaceService,
        private notificationService: NotificationService
    ) {
        this.getAllSpaces();
    }

    ngOnInit() {
        this.notificationService.notify$.subscribe(
            (notification: MatSnackBarConfig) => this.notifyUser(notification)
        );

        this.notificationService.loading$.subscribe(
            (isLoading: boolean) => (this.isLoading = isLoading)
        );
    }

    routeTo(space: any) {
        this.router.navigate(['spaces', space.sys.id], {
            replaceUrl: true
        });
    }

    private getAllSpaces(): void {
        this.spaceService.getAllSpaces().subscribe(spaces => {
            this.spaces = spaces.items;
            this.notificationService.loading$.next(false);
            this.routeToPrimarySpace(_.get(spaces.items[0], 'sys.id'));
        });
    }

    private routeToPrimarySpace(spaceId: string): void {
        this.router.navigate(['spaces', spaceId], { replaceUrl: true });
    }

    private notifyUser(notification: MatSnackBarConfig): void {
        this.snackBar.openFromComponent(
            SnackBarNotificationComponent,
            notification
        );
    }
}
