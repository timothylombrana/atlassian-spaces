import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { MatSnackBarConfig } from '@angular/material';

@Injectable({
    providedIn: 'root'
})
export class NotificationService {
    loading$: Subject<boolean> = new Subject<boolean>();
    notify$: Subject<MatSnackBarConfig> = new Subject<MatSnackBarConfig>();

    constructor() {}

    notifyUser(notificationConfig: MatSnackBarConfig): void {
        this.notify$.next(notificationConfig);
    }

    notifyUserOnSuccess(
        translation: string = 'shared.generic-success',
        className: string = 'success'
    ): void {
        this.notifyUser({
            data: { text: translation, template: 'success' },
            duration: 6000,
            panelClass: className
        });
    }

    notifyUserOnError(
        translation: string = 'shared.generic-error',
        className: string = 'error'
    ): void {
        this.notifyUser({
            data: { text: translation, template: 'error' },
            duration: 6000,
            panelClass: className
        });
    }

    notifyUserNeutral(
        translation: string = 'shared.generic-neutral',
        className: string = 'neutral'
    ): void {
        this.notifyUser({
            data: { text: translation, template: 'neutral' },
            duration: 6000,
            panelClass: className
        });
    }
}
