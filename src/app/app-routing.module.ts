import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SpacesComponent } from './spaces/spaces.component';
import { SpaceComponent } from './spaces/space/space.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/spaces'
    },
    {
        path: 'spaces',
        component: SpacesComponent,
        data: { title: 'All Spaces' },
        children: [
            {
                path: ':spaceId',
                component: SpaceComponent,
                data: { title: 'Current Space' }
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
