import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';

import { Assets, AssetsItem } from '../models/assets';
import { Entries, EntriesItem } from './../models/entries';
import { Spaces, SpacesItem } from '../models/space';
import { UsersItem, Users } from '../models/users';

@Injectable({
    providedIn: 'root'
})
export class SpaceService {
    private user: UsersItem;
    private readonly BASE_URL = 'https://spaces.proxy.beeceptor.com';

    constructor(private http: HttpClient) {}

    getAllSpaces(): Observable<Spaces> {
        return this.http.get<Spaces>(`${this.BASE_URL}/spaces`);
    }

    getSpace(spaceId: string): Observable<SpacesItem> {
        return this.http.get<SpacesItem>(`${this.BASE_URL}/spaces/${spaceId}`);
    }

    getAssets(spaceId: string): Observable<Assets> {
        return this.http.get<Assets>(
            `${this.BASE_URL}/spaces/${spaceId}/assets`
        );
    }

    getAsset(spaceId: string, assetId: string): Observable<AssetsItem> {
        return this.http.get<AssetsItem>(
            `${this.BASE_URL}/spaces/${spaceId}/assets/${assetId}`
        );
    }

    getEntries(spaceId: string): Observable<Entries> {
        return this.http.get<Entries>(
            `${this.BASE_URL}/spaces/${spaceId}/entries`
        );
    }

    getEntry(spaceId: string, entryId: string): Observable<EntriesItem> {
        return this.http.get<EntriesItem>(
            `${this.BASE_URL}/spaces/${spaceId}/entries/${entryId}`
        );
    }

    getUsers(): Observable<Users> {
        return this.http.get<Users>(`${this.BASE_URL}/users`);
    }

    getUser(userId: string): Observable<UsersItem> {
        if (this.user && this.user.sys.id === userId) {
            return of(this.user);
        }
        return this.http
            .get<UsersItem>(`${this.BASE_URL}/users/${userId}`)
            .pipe(tap((user: UsersItem) => (this.user = user)));
    }
}
