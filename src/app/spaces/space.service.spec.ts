import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpResponse, HttpClient } from '@angular/common/http';

import { MOCK_DATA } from './mock-data';
import { SpaceService } from './space.service';

function createResponse(body) {
    return of(new HttpResponse({ body }));
}

class MockHttp {
    get() {
        return createResponse([]);
    }
}

describe('SpaceService', () => {
    beforeEach(() =>
        TestBed.configureTestingModule({
            providers: [
                SpaceService,
                { provide: HttpClient, useClass: MockHttp }
            ]
        })
    );

    it('should Get all spaces', () => {
        const service: SpaceService = TestBed.get(SpaceService);
        const http: HttpClient = TestBed.get(HttpClient);
        spyOn(http, 'get').and.returnValue(
            createResponse([...MOCK_DATA.spaces.items])
        );
        service.getAllSpaces().subscribe(result => {
            expect(result.body.length).toBe(3);
            expect(result.body).toEqual(MOCK_DATA.spaces.items);
        });
    });

    it('should Get a single space', () => {
        const service: SpaceService = TestBed.get(SpaceService);
        const http: HttpClient = TestBed.get(HttpClient);
        spyOn(http, 'get').and.returnValue(
            createResponse({ ...MOCK_DATA.spaces.items[0] })
        );
        service.getSpace('yadj1kx9rmg0').subscribe(result => {
            expect(result.body.fields.title).toBe('My First Space');
            expect(result.body).toEqual(MOCK_DATA.spaces.items[0]);
        });
    });

    it('should Get all assets', () => {
        const service: SpaceService = TestBed.get(SpaceService);
        const http: HttpClient = TestBed.get(HttpClient);
        spyOn(http, 'get').and.returnValue(
            createResponse([...MOCK_DATA.assets.items])
        );
        service.getAssets('yadj1kx9rmg0').subscribe(result => {
            expect(result.body.length).toBe(3);
            expect(result.body).toEqual(MOCK_DATA.assets.items);
        });
    });

    it('should Get a single asset', () => {
        const service: SpaceService = TestBed.get(SpaceService);
        const http: HttpClient = TestBed.get(HttpClient);
        spyOn(http, 'get').and.returnValue(
            createResponse({ ...MOCK_DATA.assets.items[0] })
        );
        service
            .getAsset('yadj1kx9rmg0', 'wtrHxeu3zEoEce2MokCSi1')
            .subscribe(result => {
                expect(result.body.fields.title).toBe(
                    'Hero Collaboration Partial'
                );
                expect(result.body).toEqual(MOCK_DATA.assets.items[0]);
            });
    });

    it('should Get all entries', () => {
        const service: SpaceService = TestBed.get(SpaceService);
        const http: HttpClient = TestBed.get(HttpClient);
        spyOn(http, 'get').and.returnValue(
            createResponse([...MOCK_DATA.entries.items])
        );
        service.getEntries('yadj1kx9rmg0').subscribe(result => {
            expect(result.body.length).toBe(3);
            expect(result.body).toEqual(MOCK_DATA.entries.items);
        });
    });

    it('should Get a single entry', () => {
        const service: SpaceService = TestBed.get(SpaceService);
        const http: HttpClient = TestBed.get(HttpClient);
        spyOn(http, 'get').and.returnValue(
            createResponse({ ...MOCK_DATA.entries.items[0] })
        );
        service
            .getEntry('yadj1kx9rmg0', '5KsDBWseXY6QegucYAoacS1')
            .subscribe(result => {
                expect(result.body.sys.id).toBe('5KsDBWseXY6QegucYAoacS1');
                expect(result.body).toEqual(MOCK_DATA.entries.items[0]);
            });
    });

    it('should Get all users', () => {
        const service: SpaceService = TestBed.get(SpaceService);
        const http: HttpClient = TestBed.get(HttpClient);
        spyOn(http, 'get').and.returnValue(
            createResponse([...MOCK_DATA.users.items])
        );
        service.getUsers().subscribe(result => {
            expect(result.body.length).toBe(2);
            expect(result.body).toEqual(MOCK_DATA.users.items);
        });
    });

    it('should Get a single user', () => {
        const service: SpaceService = TestBed.get(SpaceService);
        const http: HttpClient = TestBed.get(HttpClient);
        spyOn(http, 'get').and.returnValue(
            createResponse({ ...MOCK_DATA.users.items[0] })
        );
        service.getUser('4FLrUHftHW3v2BLi9fzfjU').subscribe(result => {
            expect(result.body.fields.name).toBe('Alana Atlassian');
            expect(result.body).toEqual(MOCK_DATA.users.items[0]);
        });
    });
});
