import { Component, OnInit, ViewChild, Input } from '@angular/core';
import {
    MatTableDataSource,
    MatPaginator,
    MatSort,
    Sort
} from '@angular/material';

import { Assets, AssetsItem } from '../../models/assets';

@Component({
    selector: 'app-space-assets-table',
    templateUrl: './space-assets-table.component.html',
    styleUrls: ['./space-assets-table.component.scss']
})
export class SpaceAssetsTableComponent implements OnInit {
    isLoadingResults = false;
    displayedColumns: string[] = [
        'title',
        'contentType',
        'fileName',
        'createdBy',
        'updatedBy',
        'lastUpdated'
    ];
    dataSource: MatTableDataSource<AssetsItem>;

    @Input() assetData: Assets;

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;

    constructor() {}

    ngOnInit() {
        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(this.assetData.items);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    sortData(sort: Sort) {
        this.isLoadingResults = true;
        let sortedData: Array<AssetsItem>;
        const data = [...this.assetData.items];
        if (!sort.active || sort.direction === '') {
            this.dataSource = new MatTableDataSource(data);
            return;
        }

        sortedData = data.sort((a, b) => {
            const isAsc = sort.direction === 'asc';
            switch (sort.active) {
                case 'title':
                    return compare(a.fields.title, b.fields.title, isAsc);
                case 'contentType':
                    return compare(
                        a.fields.contentType,
                        b.fields.contentType,
                        isAsc
                    );
                case 'fileName':
                    return compare(a.fields.fileName, b.fields.fileName, isAsc);
                case 'createdBy':
                    return compare(a.sys.createdBy, b.sys.createdBy, isAsc);
                case 'updatedBy':
                    return compare(a.sys.updatedBy, b.sys.updatedBy, isAsc);
                case 'lastUpdated':
                    return compare(a.sys.updatedAt, b.sys.updatedAt, isAsc);
                default:
                    return 0;
            }
        });
        this.dataSource = new MatTableDataSource(sortedData);
        this.isLoadingResults = false;
    }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
