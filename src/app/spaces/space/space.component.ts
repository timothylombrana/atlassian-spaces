import * as _ from 'lodash';
import { ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { forkJoin, Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

import { Assets } from 'src/app/models/assets';
import { AssetsItem } from './../../models/assets';
import { Entries } from 'src/app/models/entries';
import { EntriesItem } from './../../models/entries';
import { NotificationService } from 'src/app/notification.service';
import { SpaceService } from '../space.service';
import { SpacesItem } from '../../models/space';
import { UsersItem } from 'src/app/models/users';

@Component({
    selector: 'app-space',
    templateUrl: './space.component.html',
    styleUrls: ['./space.component.scss']
})
export class SpaceComponent implements OnInit {
    assets: Assets;
    entries: Entries;
    hasAssets = false;
    hasEntries = false;
    isLoading = true;
    space: SpacesItem;

    constructor(
        private activeRoute: ActivatedRoute,
        private spaceService: SpaceService,
        private notificationService: NotificationService
    ) {}

    ngOnInit() {
        this.activeRoute.params.subscribe((params: Params) =>
            this.getAllSpaceData(params.spaceId)
        );
    }

    private getAllSpaceData(spaceId: string): void {
        this.isLoading = true;
        // Observable for space data.
        const space$ = this.spaceService.getSpace(spaceId);
        // Observable for asset data.
        const assets$ = this.spaceService.getAssets(spaceId);
        // Observable for entries data.
        const entries$ = this.spaceService.getEntries(spaceId);
        // Queue all observables
        forkJoin([space$, assets$, entries$]).subscribe(
            (group: [SpacesItem, Assets, Entries]) => this.handleSuccess(group),
            (error: HttpErrorResponse) => this.handleError(error)
        );
    }

    private handleSuccess(group: [SpacesItem, Assets, Entries]): void {
        this.space = group[0];
        this.assets = group[1];
        this.entries = group[2];
        this.hasAssets = !!this.assets.total;
        this.hasEntries = !!this.entries.total;

        if (this.hasAssets) {
            this.formatUserData(this.assets);
        }
        if (this.hasEntries) {
            this.formatUserData(this.entries);
        }
        // Observable for retrieving proper user name.
        this.spaceService
            .getUser(this.space.sys.createdBy)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe(
                (user: UsersItem) =>
                    (this.space.sys.createdBy = user.fields.name)
            );
    }

    private handleError(error: HttpErrorResponse): void {
        this.notificationService.notifyUserOnError(
            'Error while trying to retrieve space data, please try reloading the page or contact your administrator.'
        );
        console.error(error.message || error);
    }

    private formatUserData(data: Assets | Entries): void {
        this.getUserData(data).subscribe(
            (users: Array<UsersItem>) => {
                this.setUserData(data, users);
            },
            (error: HttpErrorResponse) => {
                this.notificationService.notifyUserOnError(
                    'Error while trying to retrieve user data, please try reloading the page or contact your administrator.'
                );
                console.error(error.message || error);
            }
        );
    }

    private getUserData(data: Assets | Entries): Observable<Array<UsersItem>> {
        let createdByUser: boolean;
        let updatedByUser: boolean;
        const requestQueue = [];
        const users: Array<string> = [];
        data.items.forEach((item: AssetsItem | EntriesItem) => {
            createdByUser = users.indexOf(item.sys.createdBy) === -1;
            updatedByUser = users.indexOf(item.sys.updatedBy) === -1;
            if (createdByUser) {
                users.push(item.sys.createdBy);
            }
            if (updatedByUser && createdByUser !== updatedByUser) {
                users.push(item.sys.updatedBy);
            }
        });
        users.forEach((user: string) => {
            requestQueue.push(this.spaceService.getUser(user));
        });

        return forkJoin(requestQueue);
    }

    private setUserData(data: Assets | Entries, users: Array<UsersItem>): void {
        data.items.forEach((item: AssetsItem | EntriesItem) => {
            users.forEach((user: UsersItem) => {
                if (user.sys.id === item.sys.createdBy) {
                    item.sys.createdBy = user.fields.name;
                }
                if (user.sys.id === item.sys.updatedBy) {
                    item.sys.updatedBy = user.fields.name;
                }
            });
        });
    }
}
