import { Component, OnInit, ViewChild, Input } from '@angular/core';
import {
    MatPaginator,
    MatSort,
    MatTableDataSource,
    Sort
} from '@angular/material';

import { Entries, EntriesItem } from '../../models/entries';

@Component({
    selector: 'app-space-entries-table',
    templateUrl: './space-entries-table.component.html',
    styleUrls: ['./space-entries-table.component.scss']
})
export class SpaceEntriesTableComponent implements OnInit {
    isLoadingResults = false;
    displayedColumns: string[] = [
        'title',
        'summary',
        'createdBy',
        'updatedBy',
        'lastUpdated'
    ];
    dataSource: MatTableDataSource<EntriesItem>;

    @Input() entriesData: Entries;

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;

    constructor() {}

    ngOnInit() {
        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(this.entriesData.items);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    sortData(sort: Sort) {
        this.isLoadingResults = true;
        let sortedData: Array<EntriesItem>;
        const data = [...this.entriesData.items];
        if (!sort.active || sort.direction === '') {
            this.dataSource = new MatTableDataSource(data);
            return;
        }

        sortedData = data.sort((a, b) => {
            const isAsc = sort.direction === 'asc';
            switch (sort.active) {
                case 'title':
                    return compare(a.fields.title, b.fields.title, isAsc);
                case 'summary':
                    return compare(a.fields.summary, b.fields.summary, isAsc);
                case 'createdBy':
                    return compare(a.sys.createdBy, b.sys.createdBy, isAsc);
                case 'updatedBy':
                    return compare(a.sys.updatedBy, b.sys.updatedBy, isAsc);
                case 'lastUpdated':
                    return compare(a.sys.updatedAt, b.sys.updatedAt, isAsc);
                default:
                    return 0;
            }
        });
        this.dataSource = new MatTableDataSource(sortedData);
        this.isLoadingResults = false;
    }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
