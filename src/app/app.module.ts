import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LoaderComponent } from './loader/loader.component';
import { MaterialModule } from './material.module';
import { SnackBarNotificationComponent } from './snack-bar-notification/snack-bar-notification.component';
import { SpaceAssetsTableComponent } from './spaces/space-assets-table/space-assets-table.component';
import { SpaceComponent } from './spaces/space/space.component';
import { SpaceEntriesTableComponent } from './spaces/space-entries-table/space-entries-table.component';
import { SpacesComponent } from './spaces/spaces.component';
import { SpaceService } from './spaces/space.service';

@NgModule({
    declarations: [
        AppComponent,
        SpacesComponent,
        SpaceComponent,
        SpaceEntriesTableComponent,
        SpaceAssetsTableComponent,
        SnackBarNotificationComponent,
        LoaderComponent
    ],
    entryComponents: [SnackBarNotificationComponent, LoaderComponent],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule,
        MaterialModule,
        FlexLayoutModule
    ],
    providers: [SpaceService],
    bootstrap: [AppComponent]
})
export class AppModule {}
