import { Component, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material';

@Component({
    selector: 'app-snack-bar-notification',
    templateUrl: './snack-bar-notification.component.html',
    styleUrls: ['./snack-bar-notification.component.scss']
})
export class SnackBarNotificationComponent {
    constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any) {}
}
