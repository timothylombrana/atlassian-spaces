export interface Spaces {
    sys: SpacesSys;
    total: number;
    skip: number;
    limit: number;
    items: SpacesItem[];
}

export interface SpacesItem {
    fields: SpacesFields;
    sys: SpaceItemSys;
}

export interface SpacesFields {
    title: string;
    description: string;
}

export interface SpaceItemSys {
    id: string;
    type: string;
    createdAt: string;
    createdBy: string;
    updatedAt: string;
    updatedBy: string;
}

export interface SpacesSys {
    type: string;
}
