export interface Assets {
    sys: AssetsSys;
    total: number;
    skip: number;
    limit: number;
    items: AssetsItem[];
}

export interface AssetsItem {
    fields: AssetsFields;
    sys: AssetsItemSys;
}

export interface AssetsFields {
    title: string;
    contentType: string;
    fileName: string;
    upload: string;
}

export interface AssetsItemSys {
    id: string;
    type: string;
    version: number;
    space: string;
    createdAt: string;
    createdBy: string;
    updatedAt: string;
    updatedBy: string;
}

export interface AssetsSys {
    type: string;
}
