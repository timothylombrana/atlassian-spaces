export interface Users {
    sys: UsersSys;
    total: number;
    skip: number;
    limit: number;
    items: UsersItem[];
}

export interface UsersItem {
    fields: UsersFields;
    sys: UsersItemSys;
}

export interface UsersFields {
    name: string;
    role: string;
}

export interface UsersItemSys {
    id: string;
    type: string;
}

export interface UsersSys {
    type: string;
}
