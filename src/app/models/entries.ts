export interface Entries {
    sys: EntriesSys;
    total: number;
    skip: number;
    limit: number;
    items: EntriesItem[];
}

export interface EntriesItem {
    fields: EntriesFields;
    sys: EntriesItemSys;
}

export interface EntriesFields {
    title: string;
    summary: string;
    body: string;
}

export interface EntriesItemSys {
    id: string;
    type: string;
    version: number;
    space: string;
    createdAt: string;
    createdBy: string;
    updatedAt: string;
    updatedBy: string;
}

export interface EntriesSys {
    type: string;
}
